import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { HeaderComponent } from './components/header/header.component';
import { FormComponent } from './components/form/form.component';
import { ModalComponent } from './components/modal/modal.component';
import { EncuestasComponent } from './components/encuestas/encuestas.component';
import { FormEncuestasComponent } from './components/form-encuestas/form-encuestas.component';
import { DetalleEncuestaComponent } from './components/detalle-encuesta/detalle-encuesta.component';
import { LoginComponent } from './components/login/login.component';
import { EncuestaDesignerComponent } from './components/encuesta-designer/encuesta-designer.component';
import { HomeComponent } from './components/home/home.component';
import { QuienesomosComponent } from './components/quienesomos/quienesomos.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { EnqueconsisteComponent } from './components/enqueconsiste/enqueconsiste.component';
import { RespondeEncuestaComponent } from './components/responde-encuesta/responde-encuesta.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
{
path: 'usuarios',
component: UsuariosComponent
},
{
  path: 'header',
  component: HeaderComponent
},
{
  path: 'form',
  component: FormComponent
},
{
  path: 'form/:id',
  component: FormComponent
},
{
  path: 'encuestas',
  component: EncuestasComponent
},
{
  path: 'usuarios/encuestas/:id',
  component: ModalComponent
},
{
  path: 'encuestas/form',
  component: FormEncuestasComponent
},
{
  path: 'encuestas/form/:id',
  component: FormEncuestasComponent
},
{
  path: 'encuesta/detalle/:id',
  component: DetalleEncuestaComponent
},
{
  path: 'encuesta/designer/:id',
  component: EncuestaDesignerComponent
},
// {
//   path: 'encuesta/responder/:codigo',
//   component: RespondeEncuestaComponent
// },
{
  path: 'encuesta/responder/:id',
  component: RespondeEncuestaComponent
},
{
  path: 'login',
  component: LoginComponent
},
{
  path: 'home',
  component: HomeComponent
},
{
  path: 'quienes',
  component: QuienesomosComponent
},
{
  path: 'contacto',
  component: ContactoComponent
},
{
  path: 'enqueconsiste',
  component: EnqueconsisteComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
