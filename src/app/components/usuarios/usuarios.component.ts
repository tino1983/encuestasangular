import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service.js';
import swal from 'sweetalert2';


@Component({
  selector: 'app-clientes',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  providers: [UsuarioService]
})
export class UsuariosComponent implements OnInit {

  usuarios: Usuario[];

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
   this.mostrarUsuarios();
  }

  mostrarUsuarios(){
    this.usuarioService.getUsuarios().subscribe(
      response => this.usuarios = response
    );
  }

  delete(usuario: Usuario): void {
    swal.fire({
      title: 'Estas seguro?',
      text: 'No podrás volver a recuperar al usuario',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si,borralo',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.usuarioService.delete(usuario.id).subscribe(
          response => {
           this.usuarios = this.usuarios.filter(cli => cli !== usuario);
          }
        )
        swal.fire(
          'Eliminado!',
          'El usuario se ha borrado.',
          'success'
        );
      }
    });

  }

}
