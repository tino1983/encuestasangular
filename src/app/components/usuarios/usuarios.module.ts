
import { NgModule } from '@angular/core';
import { UsuariosComponent } from './usuarios.component';
import {RouterModule} from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [UsuariosComponent],
  imports: [RouterModule,BrowserModule,FormsModule],
  exports: [UsuariosComponent],
  providers: []
  
})
export class UsuariosModule { }