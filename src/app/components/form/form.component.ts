import { Component, OnInit } from '@angular/core';
import {Usuario} from '../../models/usuario';
import {NgForm, Form} from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router,ActivatedRoute,Routes } from '@angular/router';
import swal from 'sweetalert2';
import { OauthService } from 'src/app/services/oauth.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private usuario: Usuario = new Usuario();
  private titulo = 'Registro';
  private errores: string[];
  private password: string;

  constructor(
    private usuarioService: UsuarioService,
    private router: Router,
    private routes: ActivatedRoute,
    private authService: OauthService) {
    this.usuario = authService.usuario;
  }

  ngOnInit() {
    /*this.cargarUsuario();*/
  }

  public cargarUsuario(): void{
    this.routes.params.subscribe(
      params => {
        let id = params['id'];

        if (id) {
          this.usuarioService.getUsuario(id).subscribe(
              usuario => {
              this.usuario = usuario;
            }
          );
        }
      }
    );
  }

 public create(f: Form): void {
  let userNew = this.usuario;
  // userNew.userName = f.getControl('username').value;
  this.usuarioService.create(this.usuario).subscribe(
    response => {
      this.router.navigate(['/home']);
      swal.fire('Nuevo usuario', `Has sido registrado,  ${this.usuario.nombre}. Ya puedes iniciar sesión`, 'success');
    },
    err => {
      this.errores = err.error.errors;
      console.error(err.error.errors);
      console.error('Codigo de error desde el Backend ' + err.status);
    }
  );
 }

 public update(): void {
   this.usuarioService.update(this.usuario, this.usuario.id).subscribe(
     usuario => {
       this.router.navigate(['/home']);
       swal.fire('Usuario actualizado', `El usuario ${this.usuario.nombre} se ha actualizado con éxito`, 'success');
     },
     err => {
      this.errores = err.error.errors;
      console.error(err.error.errors);
      console.error('Codigo de error desde el Backend ' + err.status);
      }
   );
 }



}
