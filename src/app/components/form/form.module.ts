import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormComponent } from './form.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormComponent],
  imports: [RouterModule,FormsModule,BrowserModule],
  exports: [FormComponent],
  providers: []
  
})
export class FormModule { }