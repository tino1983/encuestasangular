import { UsuarioService } from './../../services/usuario.service';
import { EstadoEncuesta } from './../../models/estadoEncuesta';
import { EnumsServiceService } from 'src/app/services/enums-service.service';
import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { Encuesta } from 'src/app/models/encuesta';
import swal from 'sweetalert2';
import { OauthService } from 'src/app/services/oauth.service';
import { Usuario } from 'src/app/models/usuario';
import { EstadoEncuestaService } from 'src/app/services/estadoEncuesta.service';

@Component({
  selector: 'app-encuestas',
  templateUrl: './encuestas.component.html',
  styleUrls: ['./encuestas.component.css'],
  providers: [EncuestaService]
})
export class EncuestasComponent implements OnInit {
  id: number;
  encuestas: Encuesta[];
  usuario: Usuario;

  siSoyAdmin: boolean = this.authService.hasRole('ROLE_ADMIN');
  siSoyUser: boolean = this.authService.hasRole('ROLE_USER');

  listaEstados: EstadoEncuesta[] = [];

  listaUsuarios: Usuario[] = [];

  constructor(
    private encuestaService: EncuestaService,
    private enumService: EnumsServiceService,
    private estadosService: EstadoEncuestaService,
    private usuariosService: UsuarioService,
    private authService: OauthService) {
    this.usuario = new Usuario();
   }

  ngOnInit() {
    this.mostrarEncuestas();
    this.usuario = this.authService.usuario;
    this.usuariosService.getUsuarios().subscribe( u => {
      if (u) {
        this.listaUsuarios = u;
      }
    });
    this.estadosService.getEstadosEncuesta().subscribe( e => {
      this.listaEstados = e;
    });
    this.siSoyAdmin = this.authService.hasRole('ROLE_ADMIN');
    this.siSoyUser = this.authService.hasRole('ROLE_USER');
  }

  mostrarEncuestas() {
    this.encuestaService.getEncuestas().subscribe(
      response => {
        if (this.siSoyAdmin) {
          this.encuestas = response;
        } else {
          this.filtrarListaEncuestasPorIdUsuario(this.authService.usuario.id, response);
        }
      }
    );
  }

  filtrarListaEncuestasPorIdUsuario(idUsuario: number, listaEncuestas: Encuesta[]) {
    const listaFiltrada = listaEncuestas.filter( e => e.usuarioId === idUsuario);
    this.encuestas = listaFiltrada;
  }

  delete(encuesta: Encuesta): void {
    swal.fire({
      title: 'Estas seguro?',
      text: 'No podrás volver a recuperar la encuesta',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si,borrala',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.encuestaService.delete(encuesta.id).subscribe(
          response => {
           this.encuestas = this.encuestas.filter(e => e !== encuesta);
          }
        );
        swal.fire(
          'Eliminado!',
          'La encuesta se ha borrado.',
          'success'
        );
      }
    });
  }

  mostrarNombreEstado(idEstado: number): string {
    let salida = '';
    salida = this.enumService.getEstadosEncuestaStr(idEstado);
    return salida;
  }

  mostrarNombreUsuario(idUsuario: number): string {
    let salida = '';
    const listaSalida = this.listaUsuarios.filter( u => u.id === idUsuario);
    if (listaSalida.length === 1) {
      salida = listaSalida[0].nombre; // + ' ' + listaSalida[0].apellidos;
    }
    return salida;
  }

}
