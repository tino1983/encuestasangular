import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EncuestasComponent } from './encuestas.component';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { EstadoEncuestaService } from 'src/app/services/estadoEncuesta.service';



@NgModule({
  declarations: [EncuestasComponent],
  imports: [RouterModule, BrowserModule, FormsModule],
  exports: [EncuestasComponent],
  providers: [EstadoEncuestaService]

})
export class EncuestasModule { }
