import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RespondeEncuestaComponent } from './responde-encuesta.component';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { PreguntaService } from 'src/app/services/pregunta.service';
import { TipoPreguntaService } from 'src/app/services/tipoPregunta.service';
import { OpcionesPreguntaService } from 'src/app/services/opcionesPregunta.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, FormBuilder } from '@angular/forms';
import { RespuestaPreguntaService } from 'src/app/services/respuestaPregunta.service';
import { RespuestaEncuestaService } from 'src/app/services/respuestaEncuestaService';
import { RespuestaOpcionService } from 'src/app/services/respuestaOpcion.service';



@NgModule({
  declarations: [ RespondeEncuestaComponent, ],
  imports: [RouterModule, BrowserModule, FormsModule,
    CommonModule
  ],
  exports: [RespondeEncuestaComponent],
  providers: [EncuestaService, PreguntaService, TipoPreguntaService, OpcionesPreguntaService, FormBuilder,
              RespuestaEncuestaService, RespuestaPreguntaService, RespuestaOpcionService ]
})
export class RespondeEncuestaModule { }
