import { RespuestaPregunta } from './../../models/respuestaPregunta';
import { RespuestaEncuesta } from './../../models/respuestaEncuesta';
import { RespuestaPreguntaService } from './../../services/respuestaPregunta.service';
import { RespuestaEncuestaService } from './../../services/respuestaEncuestaService';
import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { PreguntaService } from 'src/app/services/pregunta.service';
import { OpcionesPreguntaService } from 'src/app/services/opcionesPregunta.service';
import { TipoPreguntaService } from 'src/app/services/tipoPregunta.service';
import { Encuesta } from 'src/app/models/encuesta';
import { Pregunta } from 'src/app/models/pregunta';
import { TipoPregunta } from 'src/app/models/tipoPregunta';
import { OpcionesPregunta } from 'src/app/models/opcionesPregunta';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { OauthService } from 'src/app/services/oauth.service';
import { GlobalService } from 'src/app/services/global.service';
import { RespuestaOpcionService } from 'src/app/services/respuestaOpcion.service';
import { RespuestaOpcion } from 'src/app/models/respuestaOpcion';

@Component({
  selector: 'app-responde-encuesta',
  templateUrl: './responde-encuesta.component.html',
  styleUrls: ['./responde-encuesta.component.css']
})
export class RespondeEncuestaComponent implements OnInit {
  // form: FormGroup;
  public ipUSER: string;
  public usuario: Usuario = new Usuario();

  public encuestaResponder: Encuesta = new Encuesta();
  public listaPreguntas: Pregunta[] = [];

  public preguntaResponder: Pregunta = new Pregunta();

  public listaTiposPregunta: TipoPregunta[] = [];
  public tipoPreguntaUsing: TipoPregunta = new TipoPregunta();

  public listaOpcionesPreguntaResponder: OpcionesPregunta[] = [];


  public listaRespuestasEncuestas: RespuestaEncuesta[] = [];

  public respuestaEncuestaActual: RespuestaEncuesta = new RespuestaEncuesta();
  public respuestaPreguntaActual: RespuestaPregunta = new RespuestaPregunta();

  public errores: string[];

  constructor(
    // private _fb: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private encuestaService: EncuestaService,
    private preguntaService: PreguntaService,
    private tiposPreguntaService: TipoPreguntaService,
    private opcionesPreguntaService: OpcionesPreguntaService,
    private usuariosService: UsuarioService,
    private authService: OauthService,
    private glovalService: GlobalService,
    private respuestaEncuestaService: RespuestaEncuestaService,
    private respuestaPreguntaService: RespuestaPreguntaService,
    private respuestaOpcionService: RespuestaOpcionService,
  ) {
    // this.form = _fb.group({});
    const esUserAutenticado  = this.authService.isAuthenticated();
    if (esUserAutenticado) {
      this.usuario = this.authService.usuario;
    }
   }


  ngOnInit() {
    this.cargarDatosIniciales();
    this.recuperarIpAddres();
    this.mostrarMensajeIp();
  }

  recuperarIpAddres() {
    this.glovalService.getIPAddress().subscribe((res: any) => {
      this.ipUSER = res.ip;
    });
  }

  mostrarMensajeIp() {
    const ipAPI = '//api.ipify.org?format=json';

    swal.queue([{
      title: 'VoxMetric',
      confirmButtonText: 'Mostrar mi IP pública',
      text:
        'VoxMetric usará su IP pública ' +
        'para registrar la respuesta',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return fetch(ipAPI)
          .then(response => response.json())
          .then(data => swal.insertQueueStep(data.ip))
          .catch(() => {
            swal.insertQueueStep({
              icon: 'error',
              title: 'No fue posible acceder a su IP pública'
            });
          });
      }
    }]);
  }

  cargarDatosIniciales(): void {
    this.routes.params.subscribe(
      params => {
        // const cod = params.codigo;
        const id = params.id;
        if (id) {
          this.encuestaService.getEncuesta(id).subscribe( e => {
            if (e && e.estadoEncuestaId === 2) {
              let idUser = this.usuario.id;
              this.cargarEncuestaConPreguntas(id);
            } else {
              swal.fire('Lo sentimos', 'La encuesta ' + e.codigo + ' no está publicada', 'info');
              this.router.navigate(['/home' ]);
            }
          });
        }
      }
    );
  }

  cargarEncuestaConPreguntas(idEncuesta: number): void {
    const encuestaDesignedObs = this.encuestaService.getEncuesta(idEncuesta);
    const preguntasEncuestaObs = this.preguntaService.getPreguntasEncuestaId(idEncuesta);
    const respuestasEncuestaObs = this.respuestaEncuestaService.getRespuestasEncuestaByEncuestaId(idEncuesta);
    forkJoin([encuestaDesignedObs, preguntasEncuestaObs, respuestasEncuestaObs]).subscribe(responseList => {
      // Encuesta
      const encuesta = responseList[0];
      if (encuesta) {
        if (encuesta.estadoEncuestaId === 2) {
          this.encuestaResponder = encuesta;
        } else {
          swal.fire('Lo sentimos', 'La encuesta ' + encuesta.codigo + ' no está publicada', 'info');
          this.router.navigate(['/home' ]);
        }
      }
      // Preguntas
      const preguntas = responseList[1];
      if (preguntas) {
        this.listaPreguntas = preguntas;
        if (preguntas.length > 0) {
          this.preguntaResponder = preguntas[0];
          preguntas[0].enEdicion = true;
          const idtipo = preguntas[0].tipoPreguntaId;
          this.cargarTiposPregunta(idtipo);
          this.cargarOpcionesPregunta(preguntas[0].id);
        } else {
          this.cargarTiposPregunta(1);
        }
      }
      // RespuestasEncuesta
      const respuestasEnc = responseList[2];
      if (respuestasEnc) {
        this.listaRespuestasEncuestas = respuestasEnc;
      }
    });
  }

  cargarOpcionesPregunta(preguntaId: number): void {
    this.opcionesPreguntaService.getOpcionesPreguntaByPreguntaId(preguntaId).subscribe( opciones => {
      if (opciones) {
        const listaOpciones = opciones;
        this.listaOpcionesPreguntaResponder = [];
        listaOpciones.forEach( item => item.valorTemporal = 0);
        this.listaOpcionesPreguntaResponder = listaOpciones;
      }
    });
  }

  cargarTiposPregunta(idTipoPreguntaEncuestaDesigning?: number): void {
    this.tiposPreguntaService.getTiposPregunta().subscribe( tipos => {
      if (tipos) {
        this.listaTiposPregunta = tipos;
        if (idTipoPreguntaEncuestaDesigning) {
          this.marcarTipoPreguntaEnEdicion(idTipoPreguntaEncuestaDesigning);
        }
      }
    });
  }

  marcarTipoPreguntaEnEdicion(tipoId: number): void {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.listaTiposPregunta.length; index++) {
      const element = this.listaTiposPregunta[index];
      if ( element.id ===  tipoId ) {
        this.listaTiposPregunta[index].enEdicion = true;
        this.tipoPreguntaUsing = this.listaTiposPregunta[index];
      } else {
        this.listaTiposPregunta[index].enEdicion = false;
      }
    }
  }


  irPreguntaAnterior(preguntaActual: Pregunta): void {
    const indexActual = this.listaPreguntas.indexOf(preguntaActual);
    const largo = this.listaPreguntas.length;
    if ( (indexActual - 1) === 0) {
      this.selectPregunta(this.listaPreguntas[0], 0);
    } else {
      this.selectPregunta(this.listaPreguntas[(indexActual - 1)], (indexActual - 1));
    }
  }

  irPreguntaSiguiente(preguntaActual: Pregunta): void {
    const indexActual = this.listaPreguntas.indexOf(preguntaActual);
    const largo = this.listaPreguntas.length;
    if ( (indexActual + 1) === largo) {
      // Si llegó al final ir a home
      // this.selectPregunta(this.listaPreguntas[0], 0);
      this.router.navigate(['/home']);
      swal.fire({
        position: 'center',
        icon: 'info',
        title: 'Gracias por sus respuestas',
        showConfirmButton: false,
        timer: 3000
      });
    } else {
      this.selectPregunta(this.listaPreguntas[(indexActual + 1)], (indexActual + 1));
    }
  }

  selectPregunta(pregunta: Pregunta, ind: number): void {
    this.preguntaResponder = pregunta;
    // let indice = ind;
    this.marcarPreguntaEnEdicion(pregunta.id);
    this.cargarOpcionesPregunta(pregunta.id);
    this.marcarTipoPreguntaEnEdicion(pregunta.tipoPreguntaId);
  }
  marcarPreguntaEnEdicion(preguntaId: number): void {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.listaPreguntas.length; index++) {
      const element = this.listaPreguntas[index];
      this.listaPreguntas[index].enEdicion = element.id ===  preguntaId ? true : false;
    }
  }

  getValorOpcion(opcion: OpcionesPregunta, f: NgForm, textTipo: string): number {
    const valorOp = f.value;
    const propiedad: string = textTipo + opcion.id;
    const valor =  valorOp[propiedad];
    return valor;
  }

  respuestaRadio(idRadio: number) {
    this.listaOpcionesPreguntaResponder.map( i => i.id === idRadio ? i.valorTemporal = 1 : i.valorTemporal = 0);
  }

  guardarRespuestaEncuesta(preguntaActual: Pregunta, listOpc: OpcionesPregunta[]) {
    const siInicio = this.listaPreguntas.indexOf(preguntaActual) === 0;
    if (siInicio) {
      const respEncu = new RespuestaEncuesta();
      respEncu.encuestaId = this.encuestaResponder.id;
      respEncu.ip = this.ipUSER;
      respEncu.usuarioId = this.usuario.id ? this.usuario.id : null;
      respEncu.esUsuario = this.usuario.id ? true : false;
      this.respuestaEncuestaService.create(respEncu).subscribe( resp => {
        if (resp) {
          const encuResp = resp;
          this.respuestaEncuestaActual = encuResp;
          this.guardarRespuestaPregunta(preguntaActual, listOpc);
        }
      });
    } else {
      this.guardarRespuestaPregunta(preguntaActual, listOpc);
    }
  }

  guardarRespuestaPregunta(preguntaActual: Pregunta, listOpc: OpcionesPregunta[]) {
    // Si ya tenemos respuesta de Encuesta
    if ( this.respuestaEncuestaActual.id ) {
      const respPregNew = new RespuestaPregunta();
      respPregNew.preguntaId = preguntaActual.id;
      respPregNew.respuestaEncuestaId = this.respuestaEncuestaActual.id;
      this.respuestaPreguntaService.create(respPregNew).subscribe( resp => {
        const respPreg = resp;
        this.respuestaPreguntaActual = respPreg;
        this.guardarRespuestaOpcion(listOpc);
      });
    }
  }

  guardarRespuestaOpcion(listOpc: OpcionesPregunta[]) {
    // const listOpc = this.listaOpcionesPreguntaResponder;
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < listOpc.length; index++) {
      const element = listOpc[index];
      const opcResp = new RespuestaOpcion();
      opcResp.respuestaEncuestaId = this.respuestaEncuestaActual.id;
      opcResp.respuestaPreguntaId = this.respuestaPreguntaActual.id;
      opcResp.opcionesPreguntaId = element.id;
      if (element.valorTemporal === true || element.valorTemporal === '') {
        opcResp.valorRespuestaOpcion = element.valorTemporal === true ? 1 : 0;
      } else {
        opcResp.valorRespuestaOpcion = element.valorTemporal;
      }
      this.respuestaOpcionService.create(opcResp).subscribe( resp => {
        if (resp) {
          let respOpcNew = resp;
        }
      });
    }
  }

  responderPregunta(preguntaActual: Pregunta, ft: NgForm): void {
    switch (this.preguntaResponder.tipoPreguntaId) {
      case 1: // Radio
        this.registraRespuestaTipoRadio(preguntaActual);
        break;
      case 2: // Check
        this.registraRespuestaTipoCheck(preguntaActual, ft);
        break;
      case 3: // Range
        this.registraRespuestaTipoRango(preguntaActual, ft);
        break;
      default:
        break;
    }
  }


  // Radio
  registraRespuestaTipoRadio(preguntaActual: Pregunta) {
    const listOpc = this.listaOpcionesPreguntaResponder;
    const traeValores = listOpc.filter( it => (it.valorTemporal && it.valorTemporal === 1)).length > 0;
    if (this.preguntaResponder.tipoPreguntaId === 1 && traeValores) {
      this.guardarRespuestaEncuesta(preguntaActual, listOpc);
      // tslint:disable-next-line:prefer-for-of
      for (let index = 0; index < listOpc.length; index++) {
        const element = listOpc[index];
      }

      this.irPreguntaSiguiente(preguntaActual);
    } else {
      swal.fire({
        position: 'bottom',
        icon: 'warning',
        title: 'La pregunta exige una respuesta',
        showConfirmButton: false,
        timer: 1500
      });
    }
  }

  // Check
  registraRespuestaTipoCheck(preguntaActual: Pregunta, ft: NgForm) {
    let listaChecks = ft.controls;
    const listOpc = this.listaOpcionesPreguntaResponder;
    listOpc.forEach( item => item.valorTemporal = this.getValorOpcion(item, ft, 'checkbox'));
    const traeValores = listOpc.filter( it => (it.valorTemporal && it.valorTemporal > 0)).length > 0;
    // Check
    if (this.preguntaResponder.tipoPreguntaId === 2 && traeValores) {
      this.guardarRespuestaEncuesta(preguntaActual, listOpc);
      // tslint:disable-next-line:prefer-for-of
      for (let index = 0; index < listOpc.length; index++) {
        const element = listOpc[index];
      }
      this.irPreguntaSiguiente(preguntaActual);
    } else {
      swal.fire({
        position: 'bottom',
        icon: 'warning',
        title: 'La pregunta exige una respuesta',
        showConfirmButton: false,
        timer: 1500
      });
    }
  }

  // Range
  registraRespuestaTipoRango(preguntaActual: Pregunta, ft: NgForm) {
    let listaRange = ft.controls;
    const listOpc = this.listaOpcionesPreguntaResponder;
    listOpc.forEach( item => item.valorTemporal = this.getValorOpcion(item, ft, 'range'));
    const traeValores = listOpc.filter( it => (it.valorTemporal && it.valorTemporal > 0)).length > 0;
    // Range
    if (this.preguntaResponder.tipoPreguntaId === 3 && traeValores) {
      this.guardarRespuestaEncuesta(preguntaActual, listOpc);
      // tslint:disable-next-line:prefer-for-of
      for (let index = 0; index < listOpc.length; index++) {
        const element = listOpc[index];
      }
      this.irPreguntaSiguiente(preguntaActual);
    } else {
      swal.fire({
        position: 'bottom',
        icon: 'warning',
        title: 'La pregunta exige una respuesta',
        showConfirmButton: false,
        timer: 1500
      });
    }
  }


}
