import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormEncuestasComponent } from './form-encuestas.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormEncuestasComponent],
  imports: [RouterModule, FormsModule, BrowserModule],
  exports: [FormEncuestasComponent],
  providers: []

})
export class FormEncuestasModule { }
