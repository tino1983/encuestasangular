import { EstadoEncuesta } from './../../models/estadoEncuesta';
import { Component, OnInit } from '@angular/core';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Encuesta } from 'src/app/models/encuesta';
import swal from 'sweetalert2';
import { EnumsServiceService } from 'src/app/services/enums-service.service';
import { OauthService } from 'src/app/services/oauth.service';
import { Usuario } from 'src/app/models/usuario';
import { EstadoEncuestaService } from 'src/app/services/estadoEncuesta.service';
import { Form } from '@angular/forms';

@Component({
  selector: 'app-form-encuestas',
  templateUrl: './form-encuestas.component.html',
  styleUrls: ['./form-encuestas.component.css'],
  providers: [EncuestaService]
})
export class FormEncuestasComponent implements OnInit {

  public encuesta: Encuesta = new Encuesta();
  public errores: string[];
  public titulo: string = 'Edición de Encuestas';

  usuario: Usuario = new Usuario();

  public hoy: Date = new Date();
  public hoyPlus10: Date = new Date(this.hoy.getFullYear(), this.hoy.getMonth(), this.hoy.getUTCDay() + 20);

  public listaEstados: EstadoEncuesta[] = [];

  constructor(
    private encuestaService: EncuestaService,
    private router: Router,
    private routes: ActivatedRoute,
    private authService: OauthService,
    private enumsService: EnumsServiceService,
    private estadosService: EstadoEncuestaService,
    ) { }



  ngOnInit() {
    this.cargarEncuesta();
    this.usuario = this.authService.usuario;
    this.estadosService.getEstadosEncuesta().subscribe( e => { this.listaEstados = e; });
  }

  public cargarEncuesta(): void {
    this.routes.params.subscribe(
      params => {
        const id = params.id;
        if (id) {
          this.encuestaService.getEncuesta(id).subscribe(
            encuesta => {
              this.encuesta = encuesta;
            }
          );
        }
      }
    );
  }

  public create(f: Form): void {
    // Usuario creador
    this.encuesta.usuarioId = this.usuario.id;
    // Estado
    const estadoEnEdicion = this.enumsService.estadosEncuesta.EnEdicion;
    this.encuesta.estadoEncuestaId = estadoEnEdicion;
    this.encuestaService.create(this.encuesta).subscribe(
      response => {
        this.router.navigate(['/encuestas']);
        swal.fire('Nueva encuesta', `La encuesta ${this.encuesta.tituloEncuesta} se ha creado con éxito`, 'success');
      },
      err => {
        this.errores = err.error.errors;
        console.error(err.error.errors);
        console.error('Codigo de error desde el Backend ' + err.status);
      }
    );
  }


 public update(f: Form): void {
  let encu = this.encuesta;
  let encuId = this.encuesta.id;
  this.encuestaService.update(this.encuesta, this.encuesta.id).subscribe(
    encuesta => {
      this.router.navigate(['/encuestas']);
      swal.fire('Encuesta actualizada', `La encuesta ${this.encuesta.tituloEncuesta} se ha actualizado con éxito`, 'success');
    },
    err => {
     this.errores = err.error.errors;
     console.error(err.error.errors);
     console.error('Codigo de error desde el Backend ' + err.status);
    }
  );
}

}
