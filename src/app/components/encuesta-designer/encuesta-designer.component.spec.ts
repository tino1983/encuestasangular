import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncuestaDesignerComponent } from './encuesta-designer.component';

describe('EncuestaDesignerComponent', () => {
  let component: EncuestaDesignerComponent;
  let fixture: ComponentFixture<EncuestaDesignerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncuestaDesignerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncuestaDesignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
