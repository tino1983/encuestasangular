import { async } from '@angular/core/testing';
import { OpcionesPreguntaService } from './../../services/opcionesPregunta.service';
import { TipoPregunta } from './../../models/tipoPregunta';
import { Component, OnInit, ɵregisterNgModuleType } from '@angular/core';
import { Encuesta } from 'src/app/models/encuesta';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EnumsServiceService } from 'src/app/services/enums-service.service';
import { PreguntaService } from 'src/app/services/pregunta.service';
import { forkJoin } from 'rxjs';
import { Pregunta } from 'src/app/models/pregunta';
import { TipoPreguntaService } from 'src/app/services/tipoPregunta.service';
import { OpcionesPregunta } from 'src/app/models/opcionesPregunta';
import swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { isObject } from 'util';

@Component({
  selector: 'app-encuesta-designer',
  templateUrl: './encuesta-designer.component.html',
  styleUrls: ['./encuesta-designer.component.css']
})
export class EncuestaDesignerComponent  implements OnInit {

  public encuestaDesigned: Encuesta = new Encuesta();
  public listaPreguntas: Pregunta[] = [];

  public preguntaDesigning: Pregunta = new Pregunta();

  public listaTiposPregunta: TipoPregunta[] = [];
  public tipoPreguntaUsing: TipoPregunta = new TipoPregunta();

  public listaOpcionesPreguntaDesigning: OpcionesPregunta[] = [];

  public errores: string[];
  public titulo = 'Diseño de Encuestas';

  constructor(
    private encuestaService: EncuestaService,
    private preguntaService: PreguntaService,
    private tiposPreguntaService: TipoPreguntaService,
    private opcionesPreguntaService: OpcionesPreguntaService,
    private router: Router,
    private routes: ActivatedRoute,
    private enumsService: EnumsServiceService
    ) { }



  ngOnInit() {
    this.cargarDatosIniciales();
  }

  cargarDatosIniciales(): void {
    this.routes.params.subscribe(
      params => {
        const id = params.id;
        if (id) {
          this.cargarEncuestaConPreguntas(id);
        }
      }
    );
  }

  cargarEncuestaConPreguntas(idEncuesta: number): void {
    const encuestaDesignedObs = this.encuestaService.getEncuesta(idEncuesta);
    const preguntasEncuestaObs = this.preguntaService.getPreguntasEncuestaId(idEncuesta);
    forkJoin([encuestaDesignedObs, preguntasEncuestaObs]).subscribe(responseList => {
      const encuesta = responseList[0];
      if (encuesta) {
        this.encuestaDesigned = encuesta;
      }
      const preguntas = responseList[1];
      if (preguntas) {
        this.listaPreguntas = preguntas;
        if (preguntas.length > 0) {
          this.preguntaDesigning = preguntas[0];
          preguntas[0].enEdicion = true;
          const idtipo = preguntas[0].tipoPreguntaId;
          this.cargarTiposPregunta(idtipo);
          this.cargarOpcionesPregunta(preguntas[0].id);
        } else {
          this.cargarTiposPregunta(1);
        }
      }
    });
  }

  cargarTiposPregunta(idTipoPreguntaEncuestaDesigning?: number): void {
    this.tiposPreguntaService.getTiposPregunta().subscribe( tipos => {
      if (tipos) {
        this.listaTiposPregunta = tipos;
        if (idTipoPreguntaEncuestaDesigning) {
          this.marcarTipoPreguntaEnEdicion(idTipoPreguntaEncuestaDesigning);
        }
      }
    });
  }

  selectPregunta(pregunta: Pregunta, ind: number): void {
    this.preguntaDesigning = pregunta;
    // let indice = ind;
    this.marcarPreguntaEnEdicion(pregunta.id);
    this.cargarOpcionesPregunta(pregunta.id);
    this.marcarTipoPreguntaEnEdicion(pregunta.tipoPreguntaId);
  }
  marcarPreguntaEnEdicion(preguntaId: number): void {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.listaPreguntas.length; index++) {
      const element = this.listaPreguntas[index];
      this.listaPreguntas[index].enEdicion = element.id ===  preguntaId ? true : false;
    }
  }

  selectTipoPregunta(tipo: TipoPregunta, ind: number): void {
    this.tipoPreguntaUsing = tipo;
    this.marcarTipoPreguntaEnEdicion(tipo.id);
  }

  marcarTipoPreguntaEnEdicion(tipoId: number): void {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.listaTiposPregunta.length; index++) {
      const element = this.listaTiposPregunta[index];
      if ( element.id ===  tipoId ) {
        this.listaTiposPregunta[index].enEdicion = true;
        this.tipoPreguntaUsing = this.listaTiposPregunta[index];
      } else {
        this.listaTiposPregunta[index].enEdicion = false;
      }
    }
  }

  confirmarSiAgregarPregunta(encuestaId: number, tipoPreguntaId: number): void {
    const ordenNext = this.listaPreguntas.length + 1;
    swal.fire({
      title: 'Escriba el texto de su pregunta',
      input: 'textarea',
      inputValue: '¿Texto de la pregunta ' + ordenNext + '?',
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Debe escribir algún texto!';
        } else {
          // Comienza una cadena de acciones en cascada
          const textoPreguntaNew: string = value;
          // this.agregarPregunta(encuestaId, tipoPreguntaId, textoPreguntaNew, 2);
          const tiposPreg = {
            1: 'Radio box',
            2: 'Check box',
            3: 'Rango'
          };
          swal.fire({
            title: 'Selecione el tipo de pregunta',
            input: 'radio',
            inputOptions: tiposPreg,
            inputValidator: (valueTipo) => {
              if (!valueTipo) {
                return 'Debe seleccionar alguno!';
              } else {
                const elTipo = Number(valueTipo);
                this.agregarPregunta(encuestaId, elTipo, textoPreguntaNew, 2);
              }
            }
          });
        }
      }
    });

    // swal.mixin({
    //   input: 'text',
    //   confirmButtonText: 'Next &rarr;',
    //   showCancelButton: true,
    //   progressSteps: ['1', '2', '3']
    // }).queue([
    //   {
    //     title: 'Question 1',
    //     text: 'Chaining swal2 modals is easy'
    //   },
    //   'Question 2',
    //   'Question 3'
    // ]).then((result) => {
    //   if (result.value) {
    //     const answers = JSON.stringify(result.value)
    //     swal.fire({
    //       title: 'All done!',
    //       html: `
    //         Your answers:
    //         <pre><code>${answers}</code></pre>
    //       `,
    //       confirmButtonText: 'Lovely!'
    //     });
    //   }
    // });


  }

  agregarPregunta(encuestaId: number, tipoPreguntaId: number,
                  textoPreguntaNew: string = 'Texto de la pregunta',
                  cantidadOpciones: number = 2): void {
    // Se crea una pregunta por defecto y con 2 opciones del tipo radio
    const nuevaPregunta = this.preparaPreguntaNew(textoPreguntaNew, encuestaId, tipoPreguntaId);
    let preguntaCreada: Pregunta = new Pregunta();
    let listaOpcionesCrear: OpcionesPregunta[] = [];
    this.preguntaService.create(nuevaPregunta).subscribe( p => {
      if (p) {
        preguntaCreada = p;
        this.selectPregunta(preguntaCreada, 0);
        listaOpcionesCrear = this.preparaListaOpcionesPreguntaNew(preguntaCreada, cantidadOpciones);
        // Se graban en la BBDD
        this.adicionarOpcionesParaPreguntaNueva(listaOpcionesCrear);
      }
    });
  }

  agregarOpcionPreguntaExistente(preguntaEnEdicion: Pregunta) {
    let listaOpcionesCrear: OpcionesPregunta[] = [];
    swal.fire({
      title: 'Nuevo texto de la opción de respuesta',
      input: 'textarea',
      inputValue: 'Texto respuesta',
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Debe escribir algún texto!';
        } else {
          // Actualizar
          const textoOpcionNew: string = value;
          listaOpcionesCrear = this.preparaListaOpcionesPreguntaNew(preguntaEnEdicion, 1, textoOpcionNew);
          // Se graban en la BBDD
          this.adicionarOpcionesParaPreguntaNueva(listaOpcionesCrear, false);
          this.cargarOpcionesPregunta(preguntaEnEdicion.id);
          this.selectPregunta(preguntaEnEdicion, 0);
        }
      }
    });
  }

  preparaPreguntaNew(textoPregunta: string, encuestaId: number, tipoPreguntaId: number): Pregunta {
    const preguntaSalida: Pregunta = new Pregunta();
    preguntaSalida.enEdicion = true;
    preguntaSalida.encuestaId = encuestaId;
    preguntaSalida.exigeRespuesta = true;
    preguntaSalida.orden = this.listaPreguntas !== null && this.listaPreguntas.length > 0 ?
                           this.listaPreguntas[this.listaPreguntas.length - 1].orden + 1 : 1;
    preguntaSalida.tipoPreguntaId = tipoPreguntaId;
    preguntaSalida.pregunta = textoPregunta;
    return preguntaSalida;
  }

  preparaListaOpcionesPreguntaNew(pregunta: Pregunta,
                                  cantidadOpcionesCrear: number = 1,
                                  textoOpcion: string = '',
                                  valorMinimo: number = 0,
                                  valorMaximo: number = 1,
                                  stepValor: number = 1): OpcionesPregunta[] {
    const listaOpcionesSalida: OpcionesPregunta[] = [];
    for (let index = 0; index < cantidadOpcionesCrear; index++) {
      const opcionPreguntaNew: OpcionesPregunta = new OpcionesPregunta();
      const newOrden = index + 1;
      opcionPreguntaNew.orden = newOrden;
      opcionPreguntaNew.valorMin = valorMinimo;
      opcionPreguntaNew.valorMax = pregunta.tipoPreguntaId === 3 && valorMaximo === 1 ? 10 : valorMaximo;
      opcionPreguntaNew.step = stepValor;
      opcionPreguntaNew.textoOpcion = textoOpcion.length > 0 ? textoOpcion : 'Texto respuesta ' + newOrden;
      opcionPreguntaNew.preguntaId = pregunta.id;
      // Actualizar lista
      listaOpcionesSalida.push(opcionPreguntaNew);
    }
    return listaOpcionesSalida;
  }

  adicionarOpcionesParaPreguntaNueva(listaOpciones: OpcionesPregunta[], reload: boolean = true): void {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < listaOpciones.length; i++) {
      const element = listaOpciones[i];
      const itemCreado = this.agregarOpcionPregunta(element, reload);
    }
  }

  agregarOpcionPregunta(opcionesPregunta: OpcionesPregunta, reload: boolean = true): OpcionesPregunta {
    let opcionesPreguntaCreada: OpcionesPregunta = new OpcionesPregunta();
    swal.showLoading();
    this.opcionesPreguntaService.create(opcionesPregunta).subscribe( op => {
      if (op) {
        opcionesPreguntaCreada = op;
        swal.hideLoading();
        if (reload === true) {
          this.cargarEncuestaConPreguntas(this.encuestaDesigned.id);
        }
        this.cargarOpcionesPregunta(this.preguntaDesigning.id);
      }
    });
    return opcionesPreguntaCreada;
  }

  cargarOpcionesPregunta(preguntaId: number): void {
    this.opcionesPreguntaService.getOpcionesPreguntaByPreguntaId(preguntaId).subscribe( opciones => {
      if (opciones) {
        const listaOpciones = opciones;
        this.listaOpcionesPreguntaDesigning = [];
        this.listaOpcionesPreguntaDesigning = listaOpciones;
      }
    });
  }

  editPregunta(preguntaEdit: Pregunta): void {
    swal.fire({
      title: 'Nuevo texto de su pregunta',
      input: 'textarea',
      inputValue: preguntaEdit.pregunta,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Debe escribir algún texto!';
        } else {
          // Actualizar texto
          const textoPreguntaNew: string = value;
          preguntaEdit.pregunta = textoPreguntaNew;
          this.actualizarPregunta(preguntaEdit);
          //
          // const tiposPreg = {
          //   1: 'Radio box',
          //   2: 'Check box',
          //   3: 'Rango'
          // };
          // swal.fire({
          //   title: 'Selecione el tipo de pregunta',
          //   input: 'radio',
          //   inputOptions: tiposPreg,
          //   inputValidator: (valueTipo) => {
          //     if (!valueTipo) {
          //       return 'Debe seleccionar alguno!';
          //     } else {
          //       const elTipo = Number(valueTipo);
          //       preguntaEdit.tipoPreguntaId = elTipo;
          //       this.actualizarPregunta(preguntaEdit);
          //       // this.agregarPregunta(encuestaId, elTipo, textoPreguntaNew, 2);
          //     }
          //   }
          // });
        }
      }
    });
  }

  actualizarPregunta(pregunta: Pregunta): void {
    swal.showLoading();
    this.preguntaService.update(pregunta, pregunta.id).subscribe( p => {
      swal.hideLoading();
      if (p) {
        // actualizado OK
      }
    });
  }

  confirmarSiEliminarPregunta(preguntaDelete: Pregunta): void {
    swal.fire({
      title: '¿Desea eliminar la pregunta ' + preguntaDelete.orden + '?',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        // Comienza una cadena de acciones en cascada
        this.eliminarPregunta(preguntaDelete.id);
        swal.fire(
          'Pregunta eliminada',
          'Se ha eliminado la pregunta y sus opciones',
          'success'
        );
      }
    });
  }

  eliminarPregunta(preguntaId: number): void {
    swal.showLoading();
    this.preguntaService.delete(preguntaId).subscribe( p => {
      const itemEliminado = p;
      swal.hideLoading();
      this.cargarEncuestaConPreguntas(this.encuestaDesigned.id);
    });
  }

  eliminarOpcionPregunta(opcionesPreguntaId: number) {
    if (this.listaOpcionesPreguntaDesigning.length > 1) {
      swal.showLoading();
      this.opcionesPreguntaService.delete(opcionesPreguntaId).subscribe( op => {
        const itemEliminado = op;
        swal.hideLoading();
        this.cargarOpcionesPregunta(this.preguntaDesigning.id);
        // this.cargarEncuestaConPreguntas(this.encuestaDesigned.id);
      });
    } else {
      swal.fire(
        'No se puede eliminar',
        'No se permiten preguntas sin opciones',
        'warning'
      );
    }

  }

  editOpcion(opcionesPreguntaEdit: OpcionesPregunta) {
    swal.fire({
      title: 'Nuevo texto de la opción respuesta',
      input: 'textarea',
      inputValue: opcionesPreguntaEdit.textoOpcion,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Debe escribir algún texto!';
        } else {
          // Actualizar
          const textoOpcionNew: string = value;
          opcionesPreguntaEdit.textoOpcion = textoOpcionNew;
          const tipo = this.tipoPreguntaUsing.id;
          // Si es de tipo rango
          if (tipo === 3) {
            this.configurarTipoRango(opcionesPreguntaEdit);
          } else {
            this.actualizarOpcionPregunta(opcionesPreguntaEdit);
          }
        }
      }
    });
  }

  configurarTipoRango(opcion: OpcionesPregunta) {
    const rangoMax = {
      5: '5',
      10: '10',
      100: '100 step:1',
      110: '100 step:10'
    };
    swal.fire({
      title: 'Selecione el valor máximo del rango',
      input: 'radio',
      inputOptions: rangoMax,
      inputValidator: (value) => {
        if (!value) {
          return 'Debe seleccionar alguno!';
        } else {
          const elRango = Number(value);
          opcion.valorMax = elRango === 110 ? 100 : elRango;
          opcion.step = elRango === 110 ? 10 : 1;
          this.actualizarOpcionPregunta(opcion);
        }
      }
    });
  }


  actualizarOpcionPregunta(opcion: OpcionesPregunta): void {
    swal.showLoading();
    this.opcionesPreguntaService.update(opcion, opcion.id).subscribe( op => {
      swal.hideLoading();
      if (op) {
        // actualizado OK
      } else {
        swal.fire('Error', 'No se puedo actualizar', 'error');
      }
    });
  }

  siguientePregunta(preguntaActual: Pregunta): void {
    const indexActual = this.listaPreguntas.indexOf(preguntaActual);
    const largo = this.listaPreguntas.length;
    if ( (indexActual + 1) === largo) {
      this.selectPregunta(this.listaPreguntas[0], 0);
    } else {
      this.selectPregunta(this.listaPreguntas[(indexActual + 1)], (indexActual + 1));
    }
  }

  getValorOpcion(opcion: OpcionesPregunta, f: NgForm): number {
    const valorOp = f.value;
    const propiedad: string = 'op_' + opcion.id;
    const bb =  valorOp[propiedad];
    return bb;
    // return (opcion.valorMax + ' ' + opcion.id).toString();
  }


}

