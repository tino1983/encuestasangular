import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EncuestaDesignerComponent } from './encuesta-designer.component';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { PreguntaService } from 'src/app/services/pregunta.service';
import { TipoPreguntaService } from 'src/app/services/tipoPregunta.service';
import { OpcionesPreguntaService } from 'src/app/services/opcionesPregunta.service';



@NgModule({
  declarations: [EncuestaDesignerComponent],
  imports: [RouterModule, BrowserModule, FormsModule,
    CommonModule
  ],
  exports: [EncuestaDesignerComponent],
  providers: [EncuestaService, PreguntaService, TipoPreguntaService, OpcionesPreguntaService]
})
export class EncuestaDesignerModule { }
