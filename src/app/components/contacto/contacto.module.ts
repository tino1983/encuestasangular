import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ContactoComponent } from './contacto.component';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ContactoComponent],
  imports: [RouterModule,BrowserModule,FormsModule],
  exports: [ContactoComponent],
  providers: []
  
})
export class ContactoModule { }