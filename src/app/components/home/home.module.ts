import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [HomeComponent],
  imports: [RouterModule,BrowserModule,FormsModule],
  exports: [HomeComponent],
  providers: []
})
export class HomeModule { }
