import { Encuesta } from './../../models/encuesta';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { EnumsServiceService } from 'src/app/services/enums-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public listaEncuestas: Encuesta[] = [];

  constructor(
    private router: Router,
    private encuestaService: EncuestaService,
    private enumService: EnumsServiceService,
  ) { }

  ngOnInit() {
    this.mostrarEncuestas();

  }

  mostrarEncuestas() {
    this.encuestaService.getEncuestas().subscribe(
      response => {
          this.listaEncuestas = response;
      }
    );
  }

  comprobarCodigoEncuesta(cod: string) {
    const encu = this.listaEncuestas.filter(e => e.codigo === cod && e.estadoEncuestaId === 2);
    if (encu && encu.length === 1) {
      this.router.navigate(['/encuesta/responder/' + encu[0].id ]);
    } else {
      swal.fire('Lo sentimos', 'La encuesta ' + cod + ' no está publicada', 'info');
    }
  }

  responder( f: NgForm) {
    const valido = f.valid;
    if (!valido) {
      swal.fire('Error', 'El código de la encuesta  es de 5 posiciones', 'error');
    } else {
      const valorCod = f.value;
      // const ruta =  valorCod['codigo'];
      // this.router.navigate(['/encuesta/responder/' + valorCod.codigo]);
      this.comprobarCodigoEncuesta(valorCod.codigo);
      // swal.fire('OK', 'Código de la encuesta: ' + valorCod.codigo, 'success');
    }
  }

}
