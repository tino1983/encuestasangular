import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnqueconsisteComponent } from './enqueconsiste.component';

describe('EnqueconsisteComponent', () => {
  let component: EnqueconsisteComponent;
  let fixture: ComponentFixture<EnqueconsisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnqueconsisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnqueconsisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
