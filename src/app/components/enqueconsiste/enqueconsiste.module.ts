import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EnqueconsisteComponent } from './enqueconsiste.component';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [EnqueconsisteComponent],
  imports: [RouterModule, BrowserModule, FormsModule],
  exports: [EnqueconsisteComponent],
  providers: []

})
export class EnqueconsisteModule { }