import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import swal from 'sweetalert2';
import { OauthService } from 'src/app/services/oauth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: Usuario;

  constructor(private authService: OauthService, private router: Router) {
    this.usuario = new Usuario();
  }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      swal.fire('Login', `Hola ${this.authService.usuario.userName} ya estás autenticado!`, 'info');
      this.router.navigate(['/encuestas']);
    }
  }

  login(): void {
    console.log(this.usuario);
    if (this.usuario.userName == null || this.usuario.password == null) {
      swal.fire('Error login', 'Username o Password vacías!', 'error');
      return;
    }
    this.authService.login(this.usuario).subscribe(response => {
      console.log(response);
      this.authService.guardarUsuario(response.access_token);
      this.authService.guardarToken(response.access_token);
      let usuario = this.authService.usuario;
      this.router.navigate(['/home']);
      swal.fire('Login', `Hola ${usuario.userName}, has iniciado sesión con éxito!`, 'success');
      }, err => {
        if (err.status === 400) {
          swal.fire('Error Login', 'Usuario o clave incorrectas!', 'error');
        }
      }
    );
  }



}
