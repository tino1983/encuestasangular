import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [LoginComponent],
  imports: [RouterModule, BrowserModule, FormsModule],
  exports: [LoginComponent],
  providers: []

})
export class LoginModule { }
