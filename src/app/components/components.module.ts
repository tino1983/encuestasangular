
import { NgModule } from '@angular/core';
import {FooterModule} from './footer/footer.module';
import {HeaderModule} from './header/header.module';
import { BrowserModule } from '@angular/platform-browser';

import { UsuariosModule } from './usuarios/usuarios.module';

import { FormModule } from './form/form.module';

import { ModalModule } from './modal/modal.module';
import { EncuestasModule } from './encuestas/encuestas.module';

import { FormEncuestasModule } from './form-encuestas/form-encuestas.module';

import { DetalleEncuestaModule } from './detalle-encuesta/detalle-encuesta.module';
import { LoginModule } from './login/login.module';
import { EncuestaDesignerModule } from './encuesta-designer/encuesta-designer.module';
import { HomeModule } from './home/home.module';
import { QuienesSomosModule } from './quienesomos/quienesomos.module';
import { ContactoModule } from './contacto/contacto.module';
import { EnqueconsisteModule } from './enqueconsiste/enqueconsiste.module';
import { RespondeEncuestaModule } from './responde-encuesta/responde-encuesta.module';





@NgModule({
  imports: [ FooterModule, HeaderModule, UsuariosModule, BrowserModule, FormModule, ModalModule,
            EncuestasModule, FormEncuestasModule, DetalleEncuestaModule, LoginModule,
            EncuestaDesignerModule, HomeModule, QuienesSomosModule, ContactoModule, EnqueconsisteModule,
            RespondeEncuestaModule],
  exports: [ FooterModule, HeaderModule, UsuariosModule, BrowserModule, FormModule, ModalModule,
            EncuestasModule, FormEncuestasModule, DetalleEncuestaModule, LoginModule,
            EncuestaDesignerModule, HomeModule, QuienesSomosModule, ContactoModule, EnqueconsisteModule,
            RespondeEncuestaModule],
  providers: [],
  declarations: []

})

export class ComponentsModule {}
