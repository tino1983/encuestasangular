import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DetalleEncuestaComponent } from './detalle-encuesta.component';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [DetalleEncuestaComponent],
  imports: [RouterModule,BrowserModule,FormsModule],
  exports: [DetalleEncuestaComponent],
  providers: []
  
})
export class DetalleEncuestaModule { }