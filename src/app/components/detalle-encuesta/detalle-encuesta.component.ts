import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { Encuesta } from 'src/app/models/encuesta';

@Component({
  selector: 'app-detalle-encuesta',
  templateUrl: './detalle-encuesta.component.html',
  styleUrls: ['./detalle-encuesta.component.css'],
  providers: [EncuestaService]
})
export class DetalleEncuestaComponent implements OnInit {

  private encuesta:Encuesta= new Encuesta();

  constructor(private encuestaService:EncuestaService,private router:Router,private routes:ActivatedRoute) { }

  ngOnInit() {
    this.cargarEncuesta();
  }

  public cargarEncuesta():void {
    this.routes.params.subscribe(
      params => {
        let id = params['id'];
        if(id){
          this.encuestaService.getEncuesta(id).subscribe(
            encuesta => {
              this.encuesta=encuesta;
            }
          )
        }
      }
    )
  }



}
