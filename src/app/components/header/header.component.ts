import { Component } from '@angular/core';
import swal from 'sweetalert2';
import { OauthService } from 'src/app/services/oauth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {


  constructor(private authService: OauthService, private router: Router) { }
  logout(): void {
    const userName = this.authService.usuario.userName;
    this.authService.logout();
    swal.fire('Logout', `Hola ${userName}, has cerrado sesión con éxito!`, 'success');
    this.router.navigate(['/login']);
  }

}
