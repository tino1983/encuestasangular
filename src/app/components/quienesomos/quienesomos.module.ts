import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {QuienesomosComponent } from './quienesomos.component';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [QuienesomosComponent],
  imports: [RouterModule,BrowserModule,FormsModule],
  exports: [QuienesomosComponent],
  providers: []
  
})
export class QuienesSomosModule { }