import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalComponent } from './modal.component';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ModalComponent],
  imports: [RouterModule,BrowserModule,FormsModule],
  exports: [ModalComponent],
  providers: []
  
})
export class ModalModule { }