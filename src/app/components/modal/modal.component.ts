import { Component, OnInit } from '@angular/core';
import {Usuario} from '../../models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router,ActivatedRoute,Routes } from '@angular/router';
import {Encuesta} from '../../models/encuesta';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { VirtualTimeScheduler } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  providers: [UsuarioService, EncuestaService]
})

export class ModalComponent implements OnInit {

  private id: number;
  public encuestas: Encuesta[];




  constructor(private usuarioService: UsuarioService,
              private router: Router ,
              private routes: ActivatedRoute,
              private encuestaService: EncuestaService) {
  }

  ngOnInit() {
    this.cargarId();
    this.descargarEncuestas();
  }

  public descargarEncuestas() {
    this.encuestaService.getEncuestas().subscribe(
      encuestas => {
          this.encuestas = encuestas;
          this.encuestas = this.encuestas.filter(encuesta => encuesta.usuarioId === this.id );
      }
    );
  }

  public goToUsuarios(): void {
    this.router.navigate(['/usuarios']);
  }

  public cargarId(): void {
    this.routes.params.subscribe(
      params => {
        this.id = params[ 'id' ];
      }
    );
  }


}
