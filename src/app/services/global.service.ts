import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class GlobalService {

  public estadoRespondiendo = false;

  constructor(
    private http: HttpClient ) {
  }


  public getIPAddress() {
    return this.http.get('http://api.ipify.org/?format=json');
  }

  getEstadoRespondiendo(): boolean {
    return this.estadoRespondiendo;
  }

  setEstadoRespondiendo(estado: boolean): void {
    if (estado !== undefined && estado !== null && typeof(estado) === 'boolean' ) {
      this.estadoRespondiendo = estado;
    }
  }

}
