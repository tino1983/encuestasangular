import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';

import { Observable, throwError } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';
import { Encuesta } from '../models/encuesta';
import { OauthService } from './oauth.service';
import { ListaDistribucion } from '../models/listaDistribucion';
import { ApiConfigurationService } from '../api-configuration.service';


@Injectable()
export class ListaDistribucionService {

  private urlEndpoint: string = this.config.rootUrl + 'api/listasDistribucion';
  private httpHeader = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: OauthService,
    private config: ApiConfigurationService) { }

  private agregarAuthorizationHeader() {
    const token = this.authService.token;
    if (token != null) {
      return this.httpHeader.append('Authorization', 'Bearer' + token);
    }
    return this.httpHeader;
  }

  private isNoAutorizado(e): boolean {
    if (e.status === 401) {
      /* con esto hacemos logout cuando expira el token y hay que autenticar de nuevo*/
      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if (e.status === 403) {
      swal.fire('Acceso denegado', `${this.authService.usuario.userName}, no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/encuestas']);
      return true;
    }
    return false;
  }

  getListasDistribucion(): Observable<ListaDistribucion[]> {
    // return of(CLIENTES); Convertimos/Creamos nuestro flujo observable a partir de los objetos USUARIOS en local
    return this.http.get<ListaDistribucion[]>(this.urlEndpoint);
  }

  getListaDistribucion(id): Observable<ListaDistribucion> {
    return this.http.get<ListaDistribucion>(`${this.urlEndpoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/encuestas']);
        return throwError(e);
      })
    );
  }

  create(listaDistribucion: ListaDistribucion): Observable<ListaDistribucion> {
    return this.http.post<ListaDistribucion>(this.urlEndpoint, listaDistribucion, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  update(listaDistribucion: ListaDistribucion, id: number): Observable<ListaDistribucion> {
    return this.http.put<ListaDistribucion>(
      `${this.urlEndpoint}/${id}`, listaDistribucion, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<ListaDistribucion> {
    return this.http.delete<ListaDistribucion>(`${this.urlEndpoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }
}
