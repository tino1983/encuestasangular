import { Injectable } from '@angular/core';
import { RespuestaOpcion } from '../models/respuestaOpcion';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { OauthService } from './oauth.service';
import { ApiConfigurationService } from '../api-configuration.service';
import swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RespuestaOpcionService {

  private urlEndpoint: string = this.config.rootUrl + 'api/respuestasOpcion';
  private httpHeader = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: OauthService,
    private config: ApiConfigurationService) { }

  private agregarAuthorizationHeader() {
    const token = this.authService.token;
    if (token != null) {
      return this.httpHeader.append('Authorization', 'Bearer' + token);
    }
    return this.httpHeader;
  }

  private isNoAutorizado(e): boolean {
    if (e.status === 401) {
      /* con esto hacemos logout cuando expira el token y hay que autenticar de nuevo*/
      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if (e.status === 403) {
      swal.fire('Acceso denegado', `${this.authService.usuario.userName}, no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/encuestas']);
      return true;
    }
    return false;
  }

  getRespuestasOpcion(): Observable<RespuestaOpcion[]> {
    return this.http.get<RespuestaOpcion[]>(this.urlEndpoint);
  }

  getRespuestaOpcion(id: number): Observable<RespuestaOpcion> {
    return this.http.get<RespuestaOpcion>(`${this.urlEndpoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/encuestas']);
        return throwError(e);
      })
    );
  }

  getRespuestaOpcionByOpcionesPreguntaId(id: number): Observable<RespuestaOpcion[]> {
    return this.http.get<RespuestaOpcion[]>(`${this.urlEndpoint}/opcionesPregunta/${id}`,
    { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/encuestas']);
        return throwError(e);
      })
    );
  }

  getRespuestasOpcionByRespuestaPreguntaId(id: number): Observable<RespuestaOpcion[]> {
    return this.http.get<RespuestaOpcion[]>(`${this.urlEndpoint}/respuestasPregunta/${id}`,
    { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/encuestas']);
        return throwError(e);
      })
    );
  }

  create(respuestaOpcion: RespuestaOpcion): Observable<RespuestaOpcion> {
    return this.http.post<RespuestaOpcion>(this.urlEndpoint, respuestaOpcion,
      { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  update(respuestaOpcion: RespuestaOpcion, id: number): Observable<RespuestaOpcion> {
    return this.http.put<RespuestaOpcion>(`${this.urlEndpoint}/${id}`, respuestaOpcion,
    { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<RespuestaOpcion> {
    return this.http.delete<RespuestaOpcion>(`${this.urlEndpoint}/${id}`,
    { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }



}
