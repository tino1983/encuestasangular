import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';

import { Observable, throwError} from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';
import swal from 'sweetalert2';
import {catchError} from 'rxjs/operators';
import { Pregunta } from '../models/pregunta';
import { ApiConfigurationService } from '../api-configuration.service';
import { OauthService } from './oauth.service';


@Injectable()
export class PreguntaService {

private urlEndpoint = this.config.rootUrl + 'api/preguntas';
private httpHeader = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: OauthService,
    private config: ApiConfigurationService) { }

  private agregarAuthorizationHeader() {
    const token = this.authService.token;
    if (token != null) {
      return this.httpHeader.append('Authorization', 'Bearer' + token);
    }
    return this.httpHeader;
  }

  getPreguntas(): Observable<Pregunta[]> {
    return this.http.get<Pregunta[]>(this.urlEndpoint);
  }

  getPregunta(id): Observable<Pregunta> {
    return this.http.get<Pregunta>(`${this.urlEndpoint}/${id}`).pipe(
    catchError (e => {
      this.router.navigate(['/usuario']);
      swal.fire('Error', e.error.mensaje, 'error');
      return throwError(e);
    })
    );
  }

  private isNoAutorizado(e): boolean {
    if (e.status === 401) {
      /* con esto hacemos logout cuando expira el token y hay que autenticar de nuevo*/
      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if (e.status === 403) {
      swal.fire('Acceso denegado', `${this.authService.usuario.userName}, no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/encuestas']);
      return true;
    }
    return false;
  }

  getPreguntasEncuestaId(id): Observable<Pregunta[]> {
    return this.http.get<Pregunta[]>(`${this.urlEndpoint}/encuestas/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
    catchError (e => {
      // this.router.navigate(['/usuario']);
      if (this.isNoAutorizado(e)) {
        return throwError(e);
      }
      swal.fire('Error', e.error.mensaje, 'error');
      return throwError(e);
    })
    );
  }

  create(pregunta: Pregunta): Observable<Pregunta> {
    return this.http.post<Pregunta>(this.urlEndpoint, pregunta, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError (e => {
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  update(pregunta: Pregunta, id: number): Observable<Pregunta> {
    return this.http.put<Pregunta>(`${this.urlEndpoint}/${id}`, pregunta, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError (e => {
        if ( e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Pregunta> {
    return this.http.delete<Pregunta>(`${this.urlEndpoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError (e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }
}

