import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';

import { Observable, throwError} from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';
import swal from 'sweetalert2';
import {catchError} from 'rxjs/operators';
import { ApiConfigurationService } from '../api-configuration.service';
import { OauthService } from './oauth.service';


@Injectable()
export class UsuarioService {

  // hola nuevament

private urlEndpoint = this.config.rootUrl + 'api/usuarios';
// 'http://localhost:8080/api/usuarios';
private httpHeader = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: OauthService,
    private config: ApiConfigurationService) { }

  private agregarAuthorizationHeader() {
    const token = this.authService.token;
    if (token != null) {
      return this.httpHeader.append('Authorization', 'Bearer' + token);
    }
    return this.httpHeader;
  }

  private isNoAutorizado(e): boolean {
    if (e.status === 401) {
      /* con esto hacemos logout cuando expira el token y hay que autenticar de nuevo*/
      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if (e.status === 403) {
      swal.fire('Acceso denegado', `${this.authService.usuario.userName}, no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/encuestas']);
      return true;
    }
    return false;
  }

  getUsuarios(): Observable<Usuario[]> {
    // return of(CLIENTES); Convertimos/Creamos nuestro flujo observable a partir de los objetos USUARIOS en local
    return this.http.get<Usuario[]>(this.urlEndpoint);
  }

  getUsuario(id): Observable<Usuario> {
    return this.http.get<Usuario>(`${this.urlEndpoint}/${id}`).pipe(
    catchError (e => {
          this.router.navigate(['/usuario']);
          swal.fire('Error', e.error.mensaje, 'error');
          return throwError(e);
        })
    );
  }

  create(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.urlEndpoint, usuario, {headers: this.httpHeader}).pipe(
      catchError (e => {
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  update(usuario: Usuario, id: number): Observable<Usuario> {
    return this.http.put<Usuario>(`${this.urlEndpoint}/${id}`, usuario,  { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError (e => {
        if (e.status === 400) {
          swal.fire(e.error.mensaje, e.error.error, 'error');
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Usuario> {
    return this.http.delete<Usuario>(`${this.urlEndpoint}/${id}`,  { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError (e => {
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

}
