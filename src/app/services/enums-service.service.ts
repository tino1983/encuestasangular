import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class EnumsServiceService {

  public estadosEncuesta = EstadosEncuesta;

  constructor() { }

  getEstadosEncuestaStr(estadoEncuestaId: number): string {
    let salida: string;
    switch (estadoEncuestaId) {
      case this.estadosEncuesta.Activa:
        salida = 'Activa';
        break;
      case this.estadosEncuesta.Archivada:
        salida = 'Archivada';
        break;
      case this.estadosEncuesta.Cerrada:
        salida = 'Cerrada';
        break;
      case this.estadosEncuesta.EnEdicion:
        salida = 'En Edición';
        break;
      default:
        salida = 'En Edición';
        break;
    }
    return salida;
  }

}

enum EstadosEncuesta {
  EnEdicion = 1,
  Activa = 2,
  Cerrada = 3,
  Archivada = 4
}
