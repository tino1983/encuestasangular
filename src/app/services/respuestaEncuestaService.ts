import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';

import { Observable, throwError } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';
import { RespuestaEncuesta } from '../models/respuestaEncuesta';
import { OauthService } from './oauth.service';
import { ApiConfigurationService } from '../api-configuration.service';


@Injectable()
export class RespuestaEncuestaService {

  private urlEndpoint: string = this.config.rootUrl + 'api/respuestasEncuesta';
  private httpHeader = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: OauthService,
    private config: ApiConfigurationService) { }

  private agregarAuthorizationHeader() {
    const token = this.authService.token;
    if (token != null) {
      return this.httpHeader.append('Authorization', 'Bearer' + token);
    }
    return this.httpHeader;
  }

  private isNoAutorizado(e): boolean {
    if (e.status === 401) {
      /* con esto hacemos logout cuando expira el token y hay que autenticar de nuevo*/
      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if (e.status === 403) {
      swal.fire('Acceso denegado', `${this.authService.usuario.userName}, no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/encuestas']);
      return true;
    }
    return false;
  }

  getRespuestasEncuesta(): Observable<RespuestaEncuesta[]> {
    // return of(CLIENTES); Convertimos/Creamos nuestro flujo observable a partir de los objetos USUARIOS en local
    return this.http.get<RespuestaEncuesta[]>(this.urlEndpoint);
  }

  getRespuestaEncuesta(id: number): Observable<RespuestaEncuesta> {
    return this.http.get<RespuestaEncuesta>(`${this.urlEndpoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/encuestas']);
        return throwError(e);
      })
    );
  }

  getRespuestasEncuestaByEncuestaId(id: number): Observable<RespuestaEncuesta[]> {
    return this.http.get<RespuestaEncuesta[]>(`${this.urlEndpoint}/encuestas/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }
          this.router.navigate(['/encuestas']);
          return throwError(e);
        })
      );
  }



  create(respuestaEncuesta: RespuestaEncuesta): Observable<RespuestaEncuesta> {
    return this.http.post<RespuestaEncuesta>(this.urlEndpoint, respuestaEncuesta, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  update(respuestaEncuesta: RespuestaEncuesta, id: number): Observable<RespuestaEncuesta> {
    return this.http.put<RespuestaEncuesta>(`${this.urlEndpoint}/${id}`, respuestaEncuesta,
    { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status === 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<RespuestaEncuesta> {
    return this.http.delete<RespuestaEncuesta>(`${this.urlEndpoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

}
