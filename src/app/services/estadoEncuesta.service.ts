import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';

import { Observable, throwError } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';
import { RespuestaPregunta } from '../models/respuestaPregunta';
import { OauthService } from './oauth.service';
import { ApiConfigurationService } from '../api-configuration.service';
import { EstadoEncuesta } from '../models/estadoEncuesta';


@Injectable()
export class EstadoEncuestaService {

  private urlEndpoint: string = this.config.rootUrl + 'api/estadosEncuesta';
  private httpHeader = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: OauthService,
    private config: ApiConfigurationService) { }

  private agregarAuthorizationHeader() {
    const token = this.authService.token;
    if (token != null) {
      return this.httpHeader.append('Authorization', 'Bearer' + token);
    }
    return this.httpHeader;
  }

  private isNoAutorizado(e): boolean {
    if (e.status === 401) {
      /* con esto hacemos logout cuando expira el token y hay que autenticar de nuevo*/
      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if (e.status === 403) {
      swal.fire('Acceso denegado', `${this.authService.usuario.userName}, no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/encuestas']);
      return true;
    }
    return false;
  }

  getEstadosEncuesta(): Observable<EstadoEncuesta[]> {
    // return of(CLIENTES); Convertimos/Creamos nuestro flujo observable a partir de los objetos USUARIOS en local
    return this.http.get<EstadoEncuesta[]>(this.urlEndpoint);
  }

  getEstadoEncuesta(id: number): Observable<EstadoEncuesta> {
    return this.http.get<EstadoEncuesta>(`${this.urlEndpoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/encuestas']);
        return throwError(e);
      })
    );
  }


}
