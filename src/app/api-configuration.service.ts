import { Injectable } from '@angular/core';

/**
 * Global configuration for Api services
 */
@Injectable({
  providedIn: 'root'
})
export class ApiConfigurationService {
  // rootUrl = 'https://voxmetric.herokuapp.com/';
  rootUrl = 'http://localhost:8080/';
  constructor() { }
}

export interface ApiConfigurationServiceInterface {
  rootUrl?: string;
}
