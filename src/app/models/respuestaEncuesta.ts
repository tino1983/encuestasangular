

export class RespuestaEncuesta {
    id: number;
    ip: string;
    mac: string;
    esUsuario: boolean;
    usuarioId: number;
    encuestaId: number;
}
