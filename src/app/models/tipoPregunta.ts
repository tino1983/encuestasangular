

export class TipoPregunta {
    id: number;
    tipoPregunta: string;
    /*imagenTipo:byte[];*/
    imagenTipoContentType: string;
    htmlTag: string;
    accion: string;
    enEdicion = false;
}
