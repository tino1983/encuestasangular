
export class OpcionesPregunta {
    id: number;
    textoOpcion: string;
    orden: number;
    valorMin: number;
    valorMax: number;
    step: number;
    preguntaId: number;
    valorTemporal: any;
}
