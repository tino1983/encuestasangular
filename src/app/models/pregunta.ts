
export class Pregunta {
    id: number;
    pregunta: string;
    orden: number;
    exigeRespuesta: boolean;
    tipoPreguntaId: number;
    encuestaId: number;
    enEdicion = false;
}
