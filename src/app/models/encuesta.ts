import { Usuario } from './usuario';


export class Encuesta {

    id: number;
    tituloEncuesta: string;
    textoExplicacion: string;
    codigo: string;
    fechaCreacion: string;
    fechaInicio: string;
    fechaFin: string;
    codigoIdentifica: string;
    estadoEncuestaId: number;
    listaDistribucionId: number;
    usuarioId: number;
    usuarios: Usuario[] = []; /* También se puede poner así Array<Usuario> = []*/
    nombreUsuario: string;
}
