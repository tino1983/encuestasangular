

export class RespuestaOpcion {
  id: number;
  respuestaEncuestaId: number;
  valorRespuestaOpcion: number;
  textoRespuestaOpcion: string;
  opcionesPreguntaId: number;
  respuestaPreguntaId: number;
}
