import { Encuesta } from './encuesta';


export class Usuario {
id: number;
nombre: string;
apellidos: string;
createAt: string;
email: string;
userName: string;
password: string;
encuestas: Encuesta[] = [];
roles: string[] = [];

}
