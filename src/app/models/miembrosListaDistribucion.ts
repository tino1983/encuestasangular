

export class MiembrosListaDistribucion {
    id: number;
    emailMiembroLista: string;
    nombreMiembroLista: string;
    esUsuario: boolean;
    listaDistribucionId: number;
    usuarioId: number;
}
